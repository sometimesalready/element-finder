// https://stackoverflow.com/questions/5728558/get-the-dom-path-of-the-clicked-a
module.exports =
  function getPath(elm) {
    var rightArrowParents = [],
      elm,
      entry;

    for (; elm; elm = elm.parentNode) {
      entry = elm.tagName.toLowerCase();
      if (entry === "html") {
        break;
      }
      if (elm.className) {
        entry += "." + elm.className.replace(/ /g, '.');
      }
      rightArrowParents.push(entry);
    }
    rightArrowParents.reverse();
    return rightArrowParents.join(" ")
  };