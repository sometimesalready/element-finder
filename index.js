const {
  uniqWith,
  maxBy
} = require('lodash/fp')

const getPath = require('./get-path')

const isStrictEqual = uniqWith((first, second) => first === second)
const maxByCoeeficient = maxBy('matchCoefficient')

const [_platform, _file, sourceFilePath, targetFilePath, id = 'make-everything-ok-button'] = process.argv;

const bunyan = require('bunyan');
const fs = require('fs');
const {
  JSDOM
} = require('jsdom');

const logger = bunyan.createLogger({
  name: "element path finder"
});

try {
  const sourceFile = fs.readFileSync(sourceFilePath);
  const {
    window: {
      document: sourceDocument
    }
  } = new JSDOM(sourceFile);

  const sourceElement = sourceDocument.getElementById(id)

  logger.info(`Source element: ${sourceElement.outerHTML}`);

  const targetFile = fs.readFileSync(targetFilePath);
  const target = new JSDOM(targetFile);
  const {
    window: {
      document: targetDocument
    }
  } = target

  let possibleElements = []
  const sourceAttributes = Array.from(sourceElement.attributes)
  for (const atribute of sourceAttributes) {
    const {
      name,
      value
    } = atribute;
    const newPossibleElements = targetDocument.querySelectorAll(`${sourceElement.tagName}[${name}="${value}"]`);
    possibleElements = possibleElements.concat(Array.from(newPossibleElements))
  }
  possibleElements = isStrictEqual(possibleElements)
  possibleElements = possibleElements.map(element => {
    element.matchCoefficient = 1;
    return element;
  })
  for (const element of possibleElements) {
    for (const attribute of sourceAttributes) {
      const {
        name,
        value
      } = attribute;
      if (element.getAttribute(name) === value) {
        element.matchCoefficient += 1;
      }
    }
  }

  possibleElements.forEach(element => {
    const {
      outerHTML: html,
      matchCoefficient: coefficient
    } = element
    logger.info('possible element {html,coeficient}', {
      html,
      coefficient
    })
  })

  const targetElement = maxByCoeeficient(possibleElements)
  const {
    outerHTML: html,
    matchCoefficient: coefficient
  } = targetElement;
  const path = getPath(targetElement);
  logger.info('target element {html,coefficient} with {path}', {
    html,
    coefficient,
    path
  });

  console.log();

} catch (err) {
  logger.error('Error trying to find element by id', err);
}