# Element pathfinder

## How to use

```
node index.js <input_origin_file_path> <input_other_sample_file_path> <element_id>
```

There are predefined scripts to test sample files:

```
npm run start:1
npm run start:2
npm run start:3
npm run start:4
```


## TODO:

we can increase match precisions by analyzing .textContent of elements 